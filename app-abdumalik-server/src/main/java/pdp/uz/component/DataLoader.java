package pdp.uz.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pdp.uz.entity.User;
import pdp.uz.entity.enums.RoleName;
import pdp.uz.repository.RoleRepository;
import pdp.uz.repository.UserRepository;


import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(new User(

                    "2204",

                    passwordEncoder.encode("2204"),
                    "Abdumalik",
                    "Abdusattorov",
                    roleRepository.findAllByNameIn(
                            Arrays.asList(RoleName.ROLE_ADMIN,
                                    RoleName.ROLE_DIRECTOR)

                    )));
        }
//        else{
//            Optional<User> optionalUser = userRepository.findById(UUID.fromString("a0830781-f2cf-45b9-b64b-6c243040c13d"));
//            if (optionalUser.isPresent()){
//                User user = optionalUser.get();
//                user.setRoles(roleRepository.findAllByNameIn(
//                        Arrays.asList(RoleName.ROLE_ADMIN)));
//                userRepository.save(user);
//            }
//        }

    }
}

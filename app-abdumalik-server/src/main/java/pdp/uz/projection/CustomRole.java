package pdp.uz.projection;

import org.springframework.data.rest.core.config.Projection;
import pdp.uz.entity.Role;


@Projection(name = "customRole", types = {Role.class})
public interface CustomRole {
    Integer getId();
    String getName();

}

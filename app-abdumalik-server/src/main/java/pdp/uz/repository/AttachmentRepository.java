package pdp.uz.repository;




import org.springframework.data.jpa.repository.JpaRepository;
import pdp.uz.entity.Attachment;

import java.util.List;
import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    List<Attachment> findAllByNameAndSize(String name, long size);
}

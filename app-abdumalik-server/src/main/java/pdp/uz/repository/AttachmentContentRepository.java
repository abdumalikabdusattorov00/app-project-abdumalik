package pdp.uz.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import pdp.uz.entity.Attachment;
import pdp.uz.entity.AttachmentContent;

import java.util.Optional;
import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {

    AttachmentContent getByAttachment(Attachment attachment);

    Optional<AttachmentContent> findByAttachment(Attachment attachment);
}

package pdp.uz.payload;

import lombok.Data;
import pdp.uz.entity.enums.RoleName;


import java.util.UUID;

@Data
public class ReqSignUp {
    private UUID id;

    private String phoneNumber;

    private String password;

    private String firstName;

    private String lastName;

    private RoleName roleName;

}

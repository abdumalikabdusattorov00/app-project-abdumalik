package pdp.uz.entity.enums;

public enum PayStatus {
    UNPAID,
    PAID
}

package pdp.uz.entity.enums;

public enum DeadlineStatus {
    SIMPLY,
    IMPORTANT,
    URGENT
}

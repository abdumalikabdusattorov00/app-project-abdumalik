package pdp.uz.entity.enums;

public enum Gender {
    MALE,
    FEMALE,
    KIDS
}

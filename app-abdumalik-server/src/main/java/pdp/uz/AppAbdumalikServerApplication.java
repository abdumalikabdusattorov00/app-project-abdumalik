package pdp.uz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppAbdumalikServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppAbdumalikServerApplication.class, args);
    }

}
